# Solenoid bundle


This package contains multiple parts to make a solenoid be controllable via OSC.

## Requirements

- a raspberry pi or alike, needs to have an i2c bus and network (preferably Ethernet)
    - raspbian jessie
    - with [i2c enabled](https://learn.sparkfun.com/tutorials/raspberry-pi-spi-and-i2c-tutorial#i2c-on-pi)
    - with build utils installed, do `apt-get install git-core python-smbus python-liblo arduino-mk`
- an arduino (5V, I used a Pro Mini 16Mhz)
- a solenoid (I used 12V 1A push-pull solenoids, __ZYE1-0530__)
- a mosfet (I used irf520)
- a 10K resistor
- a 12V Zener Diode
- jumper cables
- a diy pcb with mounting holes

- another pc with [Tidal](http://tidal.lurk.org) installed. (I am using 0.8-dev, but 0.7 should be fine as well)

### Tools
- soldering iron
- ftdi cable (or any other means to flash your arduino)

## Setup

To obtain the source run on your __Raspberry Pi__:

```shell
git clone https://lmaa@bitbucket.org/lmaa/solenoid.git
```

then move into the source folder:

```
cd solenoid
```

Now prepare the arduino, raspberry 

## The Arduino source code

listens on i2c and converts incoming messages into solenoid pushes

__NOTE:__ building has only been tested on a raspberry pi, your mileage may vary, though the arduino code itself (sketch.ino) should compile just fine within the default Arduino IDE.

In case you want to build and flash from the raspberry pi you need the following dependencies installed:

- arduino-mk

Installation works like this:

```shell
make
```

Then to upload to the arduino, do:

```shell
make upload
```

__NOTE:__ I used a prolific ftdi cable to flash, which does not send RESET and therefore I had to press and hold the reset button when upload just until you can see the `avrdude` command appearing on the screen. then release the reset button and flashing should work _ok_.

## A python script for the raspberry pi

Listens on osc port 1234 and dispatches `velocity` osc messages sent from Tidal into i2c messages.

## a systemd unit description

Optionally you can run the osc to i2c bridge on the pi's startup.

The contained systemd unit file is to be installed on the pi like this:

__NOTE:__ you will need superuser rights to perform these actions on the pi (that is, `sudo`)

```shell
cp osc2i2c.service /etc/systemd/system/
systemctl enable /etc/systemd/system/osc2i2c.service
systemctl start osc2i2c
```