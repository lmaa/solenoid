#!/usr/bin/env python

from itertools import izip
import smbus
import time
import liblo
import sys

try:
    bus = smbus.SMBus(1)
    server = liblo.Server(1234)
except liblo.ServerError, err:
    print str(err)
    sys.exit()    
except:
    print str(err)
    sys.exit()


address = 0x04

def writeNumber(value):
    bus.write_byte(address, value)
    return -1

def dispatch(path, args, types, src):
    i = iter(args)
    paramMap = dict(izip(i, i))
    
    print "velocity '%f'" % (paramMap['velocity'])
    writeNumber(int(128*paramMap['velocity']))

server.add_method("/play2", None, dispatch)

while True:
    server.recv(100)
