#include <Wire.h>
#include <TimerOne.h>


#define SLAVE_ADDRESS 0x04

typedef enum State {
  idle,
  edge,
  falling
};

const int led = LED_BUILTIN;
int number = 0;

class Pusher {
  int pin;

  float velocity;
  float decay;
  
  State state;
  unsigned long last;

public:
  Pusher(int p) {
    pin = p;
    pinMode(pin, OUTPUT);  
    state = idle;
    decay = 0.1;
  }

  void Push(float vel) {
    switch (state) {
    case edge:
    case falling:
      break;
    case idle:
      velocity = vel;
//      Serial.print("Pushed:\t");
//      Serial.println(vel);
      last = millis();
      state = edge;
    }
  }
  
  void Update()
  {
    switch (state) {
    case edge:
      if ((millis() - last) > 10) {
//        Serial.print("[push] initial push done, pin: ");
//        Serial.println(pin);
        state = falling;
        last = millis();
        Timer1.pwm(pin, velocity * 1023);
      }
      else {
        Timer1.pwm(pin, 1023);
      }
      
      break;
    case falling:
      if ((millis() - last) > 100) {
        Timer1.disablePwm(pin);
        //      Serial.println("[push] resting again");
        state = idle;
      }
      else {
        Timer1.pwm(pin, velocity * 1023);
      }
      break;
    case idle:
      break;    
    }
  }
};

Pusher solenoid(9);

void dispatchToPWM(int byteCount) {
//  Serial.print("[i2c] received \t");
//  Serial.print(byteCount);
//  Serial.println(" bytes");
  while (Wire.available()) {
    number = Wire.read();
    solenoid.Push((float)number/128.0);
  }
}

void setup() {
  pinMode(led, OUTPUT);  

  Timer1.initialize(40);
//  Serial.begin(9600);

  Wire.begin(SLAVE_ADDRESS);
  Wire.onReceive(dispatchToPWM);

//  Serial.println("Wires connected");
}


void loop() {
  solenoid.Update();
}
